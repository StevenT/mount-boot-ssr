(ns mount-boot-srv.client
  (:require [reagent.core :as r]
            [cljs.reader]
            [re-frame.core :as re-frame]
            [re-frame-redux.core :as redux]
            [mount-boot-srv.client.core :as core]
            [mount-boot-srv.client.views :as views]
            [mount-boot-srv.client.routes :as routes]))

(defn get-data [tag]
  (-> (.getElementById js/document "server-data")
      (.getAttribute (str "data-" tag))
      (cljs.reader/read-string)))

(defn render-page []
  (r/render [views/page]
             (.getElementById js/document "app")))

(defn reload []
  (re-frame/clear-subscription-cache!)
  (render-page))

(defn ^:export init []
  (try
    (let [state (get-data "state")]
      (core/init {:state state}))
    (catch js/Error _
      (core/init {})))
  (redux/setup)
  (routes/client-init)
  (render-page))
