(ns mount-boot-srv.client.config)

(def default-title "Bamse")
(def default-description "Universal app build using ClojureScript. Uses Re-Frame framework. Renders on server and client")
(def default-image "https://robohash.org/bamse.png?set=set4")

;; (def debug? ^boolean js/goog.DEBUG)
(def debug? true)
(def ajax-timeout 1000)
(def api-server "http://localhost:3000")

;; Translations
(def languages {:en "English"
                :nl "Nederlands"
                :fr "Francais"})
