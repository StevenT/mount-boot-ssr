(ns mount-boot-srv.client.core
  (:require [re-frame.core :as re-frame]
            [mount-boot-srv.client.routes :as routes]
            [mount-boot-srv.client.events :as events]
            [mount-boot-srv.client.subs :as subs]))

(defn dev-setup []
  #?(:cljs (do (enable-console-print!)))
  (println "[App] Starting..."))

(defn init [{:keys [path state lang]}]
  (dev-setup)
  (events/register)
  (subs/register)

  #?(:clj (do (re-frame/dispatch-sync [::events/initialize-db])
              (routes/navigate-to path true))
     :cljs (if state
             (do
               (println "[App] Loaded server state")
               (re-frame/dispatch-sync [::events/set-db state]))
             (do
               (println "[App] Could not load server state")
               (re-frame/dispatch-sync [::events/initialise-db]))))

  (when lang
    (re-frame/dispatch-sync [::events/set-language lang])))
