(ns mount-boot-srv.client.routes
  (:require [re-frame.core :as re-frame :refer [dispatch dispatch-sync]]
            [bidi.bidi :as bidi]
            [mount-boot-srv.client.events :as events]
            [mount-boot-srv.client.users.events :as users]
            [mount-boot-srv.client.helpers :as helpers]
            #?@(:cljs [[pushy.core :as pushy]
                       [re-frame.core :as re-frame]])))

(def routes ["/"     {""              :home
                      "about"         :about
                      "users/new"     :user-add
                      "users"         :users
                      ["user/"  :id]  :user
                      ["user/"  :id "/edit"] :user-edit
                      true            :not-found}])

(defn parse-url [url]
  (bidi/match-route routes url))

(defn parseInt [s]
  #?(:clj (Integer/parseInt s)
     :cljs (js/parseInt s)))

(defn dispatch-route [matched-route & synchronous]
  (let [panel-name (:handler matched-route)
        user-id (get-in matched-route [:route-params :id])]
    (case panel-name
      :home  (do
               (dispatch [::events/get-readme])
               (dispatch [:set-title (name panel-name)]))
      :about (do
               (dispatch [::events/get-url])
               (dispatch [::events/get-poe])
               (dispatch [:set-title (name panel-name)]))
      :users (do
               (dispatch [::users/get-users])
               (dispatch [:set-title (name panel-name)]))
      :user  (do
               (dispatch [::users/get-user (parseInt user-id)]))
      :user-edit (do
                   (dispatch [::users/get-user (parseInt user-id)]))
      :not-found (do
                   (dispatch [:set-title (name panel-name)]))
      "")
    (if synchronous
      (dispatch-sync [:set-active-route matched-route])
      (dispatch [:set-active-route matched-route]))))

(def url-for (partial bidi/path-for routes))

(defn navigate-to [path & synchronous]
  (dispatch-route (parse-url path) synchronous))

#?(:cljs
   (def history
     (pushy/pushy dispatch-route parse-url)))

#?(:cljs
   (defn set-pushstate! [path]
     (pushy/set-token! history path)))

#?(:cljs
   (defn client-init []
     (pushy/start! history)
     (re-frame/reg-fx :navigate-to
                      (fn [path]
                        (set-pushstate! path)))))
