(ns mount-boot-srv.client.translate
  (:require [re-frame.core :as re-frame]
            [mount-boot-srv.client.subs :as subs]
            #?@(:clj [[mount-boot-srv.macro :refer [slurp-translation]]]
                :cljs [[mount-boot-srv.macro :include-macros true :refer [slurp-translation]]
                       [goog.string :as gstring]
                       [goog.string.format]])))

(def dictionary {:nl (slurp-translation :nl)
                 :fr (slurp-translation :fr)})

(defn tr [s & args]
  (let [lang (re-frame/subscribe [::subs/language])
        string (or (get-in dictionary [@lang s]) s)]
     string ;;(apply gstring/format string args)
     ))

(defn trn [strings count & args]
  (let [lang (re-frame/subscribe [::subs/language])
        [singular plural] (or (get-in dictionary [@lang strings]) strings)]
    (if (= 1 count)
      singular ;;(apply gstring/format singular (conj args count))
      plural ;;(apply gstring/format plural   (conj args count))
      )))
