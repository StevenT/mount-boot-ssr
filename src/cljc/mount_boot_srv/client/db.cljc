(ns mount-boot-srv.client.db)

(def default-db
  {:title nil
   :language :en
   :active-route nil
   :ssr-waits     (set '())
   :readme {:http-status :empty}
   :url {:http-status :empty}
   :poe {:http-status :empty}
   :user {:id nil
          :item (:http-status :empty)}
   :users {:http-status :empty}})
