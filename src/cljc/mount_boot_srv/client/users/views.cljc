(ns mount-boot-srv.client.users.views
 (:require [re-frame.core :as re-frame]
           [mount-boot-srv.client.helpers :refer [spinner user-avatar]]
           [mount-boot-srv.client.translate :refer [tr trn]]
           [mount-boot-srv.client.routes :refer [url-for navigate-to]]
           [mount-boot-srv.client.config :as config]
           [mount-boot-srv.client.subs :as subs]
           [mount-boot-srv.client.users.subs :as user-subs]
           [mount-boot-srv.client.users.events :as user-events]))

(defn full-name [user]
 (apply str (interpose " " [(:first_name user) (:last_name user)])))

(defn valid-name [name]
 (and (not-empty name)
      (< 1 (.-length name))))

(defn valid-user [user]
 (and (valid-name (:first_name user))
      (valid-name (:last_name user))))

(defn field [{:keys [user type label id class]}]
 [:div.form-group
  [:label {:for id} label]
  (if (= type :textarea)
   [:textarea.form-control {:id       id
                            :name     id
                            :value    (get @user id)
                            :rows     4
                            :class    class
                            :onChange #(swap! user assoc id (-> % .-target .-value))}]
   [:input.form-control {:type     type
                         :id       id
                         :name     id
                         :value    (get @user id)
                         :class    class
                         :onChange #(swap! user assoc id (-> % .-target .-value))}])])

(defn user-form [user]
 [:div
  [field {:user user
          :type :text
          :label (tr "First name")
          :id :first_name
          :class (if (valid-name (get @user :first_name)) "is-valid" "is-invalid")}]

  [field {:user user
          :type :text
          :label (tr "Last name")
          :id :last_name
          :class (if (valid-name (get @user :last_name)) "is-valid" "is-invalid")}]

  [field {:user user
          :type :text
          :label (tr "Title")
          :id :title}]

  [field {:user user
          :type :textarea
          :label (tr "Description")
          :id :description}]])

(defn user-add-form [user]
 [:form.col-12.col-md-6 {:action "/api/users"
                         :method :post
                         :on-submit #(do (.preventDefault %)
                                      (re-frame/dispatch [::user-events/add-user @user]))}
  [user-form user]
  [:div
   [:a.btn.btn-outline-secondary.mr-2 {:href (url-for :users)}
    (tr "Cancel")]
   [:button.btn.btn-primary.mr-2 {:type     :submit
                                  :disabled (not (valid-user @user))}
    (tr "Add")]]])


(defn user-edit-form [user]
 [:form.col-12.col-md-6 {:on-submit #(do (.preventDefault %)
                                         (re-frame/dispatch [::user-events/update-user @user]))}
  [user-form user]
  [:div
   [:a.btn.btn-outline-secondary.mr-2 {:href (url-for :user :id (:id @user))}
    (tr "Cancel")]
   [:button.btn.btn-primary.mr-2 {:type     :submit
                                  :disabled (not (valid-user @user))}
    (tr "Save")]]])

(defn user-detail [user]
  [:div
   [:div [user-avatar user]]
   [:div.mb-4 [:h3 (full-name user)]]
   [:div.row.my-2
    [:div.col-12.col-md-2.font-weight-bold [:label (tr "Title")]]
    [:div.col-12.col-md-10 (or (:title user) "-")
     ]]
   [:div.row.my-2
    [:div.col-12.col-md-2.font-weight-bold [:label (tr "Description")]]
    [:div.col-12.col-md-10 (or (:description user) "-")]]
   [:div.my-2
    [:a.btn.btn-outline-secondary.mr-2 {:href (url-for :users)}
     (tr "Back")]
    [:a.btn.btn-primary.mr-2 {:href (url-for :user-edit :id (or (:id user) 42))}
     (tr "Edit")]
    [:button.btn.btn-danger.mr-2 {:type     :button
                                  :on-click #((re-frame/dispatch [::user-events/delete-user (:id user)])
                                              (.preventDefault %))}
     (tr "Delete")]]])

(defn users-list [users]
 [:table.table.table-hover
  [:thead.thead-light
   [:tr
    [:th {:scope :col :style {:width 20}} "#"]
    [:th {:scope :col :style {:width 60}} (tr "avatar")]
    [:th {:scope :col} (tr "first name")]
    [:th {:scope :col} (tr "last name")]]]
  [:tbody
   (for [user users]
     ^{:key user}
     [:tr
      [:th {:scope :row} (:id user)]
      [:td [user-avatar user 40]]
      [:td [:a {:href (url-for :user :id (:id user))} (:first_name user)]]
      [:td [:a {:href (url-for :user :id (:id user))} (:last_name user)]]])]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn users []
 (let [users (re-frame/subscribe [::user-subs/users])
       users-loading (re-frame/subscribe [::user-subs/users-loading])]
   (fn []
       [:main.container
        [:h1 (tr "Users")]
        [:a.btn.btn-outline-secondary.my-2 {:href (url-for :user-add)}
         (tr "Add")]
        [:div
         (if @users-loading
           [spinner]
           (if (seq @users)
             [users-list @users]
             [:div (tr "No users")]))
         (when config/debug?
           [:div
            [:button.btn.btn-secondary {:type     :button
                                        :on-click #(re-frame/dispatch [::user-events/reget-users])} (tr "Reload")]])]])))

(defn user []
 (let [user (re-frame/subscribe [::user-subs/user])
       route-id (re-frame/subscribe [::subs/active-route-id])
       loading (re-frame/subscribe [::user-subs/user-loading])]
   (fn []
     [:main.container
      [:h1 (tr "User")]
      [:div
       (if @loading
         [spinner]
         (if @user
           [user-detail @user]
           [:div (tr "No user")]))
       (when config/debug?
         [:div
          [:button.btn.btn-secondary {:type     :button
                                            :on-click #(re-frame/dispatch [::user-events/reget-user @route-id])} (tr "Reload")]])]])))

(defn user-add []
 (let [user (atom {})
       loading (re-frame/subscribe [::user-subs/add-user-loading])]
   (fn []
     [:main.container
      [:h1 (tr "New user")]
      (when @loading [spinner])
      [user-add-form user]])))

(defn edit-user [id]
  (let [user (re-frame/subscribe [::user-subs/user])
        loading (re-frame/subscribe [::user-subs/update-user-loading])]
    (fn []
      [:main.container
       [:h1 (tr "Edit user")]
       (if @loading
         [spinner]
         (if @user
           [user-edit-form (atom @user)]
           [:div (tr "No user")]))])))
