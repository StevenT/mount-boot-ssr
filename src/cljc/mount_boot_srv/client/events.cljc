(ns mount-boot-srv.client.events
  (:require [re-frame.core :as re-frame]
            [mount-boot-srv.client.config :as config]
            [mount-boot-srv.client.db :as db]
            [mount-boot-srv.client.helpers :refer [reg-event-db reg-event-fx]]
            [mount-boot-srv.client.users.events]
            #?@(:clj [[clj-http.client :as http-client]
                      [cheshire.core :as cheshire]]
                :cljs [[re-frame-redux.core :as redux]
                       [day8.re-frame.http-fx :as http-fx]
                       [ajax.core :as ajax]
                       [goog.net.cookies]])))

(defn register []
  (mount-boot-srv.client.users.events/register)

  #?(:cljs (redux/register-redux-events))

  #?(:cljs (defn set-cookie! [k v]
             (.set goog.net.cookies k v)))

  #?(:clj (re-frame/reg-fx
           :set-cookie
           (fn [[key value]]))
     :cljs (re-frame/reg-fx
            :set-cookie
            (fn [[key value]]
              (set-cookie! key (name value)))))

  (reg-event-db
   ::initialize-db
   (fn [_ _]
     db/default-db))

  (reg-event-db
   ::set-db
   (fn [_ [_ new-db]]
     new-db))

  (reg-event-db
   :set-active-route
   (fn [db [_ route]]
     (assoc db :active-route route)))

  (reg-event-fx
   ::set-language
   (fn [{:keys [:db]} [_ lang]]
     {:db (assoc db :language lang)
      :set-cookie ["language" lang]}))

  (reg-event-db
   ::toggle-mobile-menu
   (fn [db _]
     (assoc db :mobile-menu-open (not (:mobile-menu-open db)))))

  (reg-event-db
   ::close-mobile-menu
   (fn [db _]
     (assoc db :mobile-menu-open false)))

  (reg-event-db
   ::toggle-language-menu
   (fn [db _]
     (assoc db :language-menu-open (not (:language-menu-open db)))))

  (reg-event-db
   ::close-language-menu
   (fn [db _]
     (assoc db :language-menu-open false)))

  (reg-event-db
   ::close-all-menus
   (fn [db _]
     (-> db
         (assoc :mobile-menu-open false)
         (assoc :language-menu-open false))))

  (re-frame/reg-fx
   :dispatch-multiple
   (fn [events]
     (let [filtered (into {} (filter (comp not nil? first) events))]
       (doall (map re-frame/dispatch filtered)))
     nil))

  (re-frame/reg-fx
   :set-title
   (fn [title]
     #?(:cljs (try
                (let [new-title (if title
                                  (str config/default-title " - " title)
                                  config/default-title)]
                  (aset js/document "title" new-title))
                (catch :default _))
        :clj nil)))

  (reg-event-fx
   :set-title
   (fn [{:keys [:db]} [_ title]]
     {:db        (assoc db :title title)
      :set-title title}))

  ;; Readme
  (reg-event-fx
   ::good-readme-result
   (fn [{:keys [:db]} [_ res]]
     {:db (assoc db :readme {:http-status :result
                             :http-result (get-in res [:readme])})}))

  (reg-event-fx
   ::bad-readme-result
   (fn [{:keys [:db]} [_ res]]
     (println "BAD" (:status-text res))
     {:db (assoc db :readme {:http-status :error
                             :http-error "error"})}))

  (reg-event-fx
   ::reget-readme
   (fn [{:keys [:db]} _]
     {:db (assoc db :readme {:http-status :empty})
      :dispatch [::get-readme]}))

  (reg-event-fx
   ::get-readme
   (fn [{:keys [:db]} _]
     (if (or (= (get-in db [:readme :http-status]) :empty)
             (= (get-in db [:readme :http-status]) :loading))
       {:db (assoc db :readme {:http-status :loading})
        :http-fx {:uri        "/api/readme"
                  :ssr-wait [:readme]
                  :on-success [::good-readme-result]
                  :on-failure [::bad-readme-result]}}
       {})))


  ;; Url
  (reg-event-fx
   ::good-url-result
   (fn [{:keys [:db]} [_ res]]
     {:db (assoc db :url {:http-status :result
                          :http-result (get-in res [:url])})}))

  (reg-event-fx
   ::bad-url-result
   (fn [{:keys [:db]} [_ res]]
     (println "BAD" (:status-text res))
     {:db (assoc db :url {:http-status :error
                          :http-error "error"})}))

  (reg-event-fx
   ::reget-url
   (fn [{:keys [:db]} _]
     {:db (assoc db :url {:http-status :empty})
      :dispatch [::get-url]}))

  (reg-event-fx
   ::get-url
   (fn [{:keys [:db]} _]
     (if (or (= (get-in db [:url :http-status]) :empty)
             (= (get-in db [:url :http-status]) :loading))
       {:db (assoc db :url {:http-status :loading})
        :http-fx {:uri        "/api/test"
                  :ssr-wait [:url]
                  :on-success [::good-url-result]
                  :on-failure [::bad-url-result]}}
       {})))


  ;; Poe
  (reg-event-fx
   ::good-poe-result
   (fn [{:keys [:db]} [_ res]]
     {:db (assoc db :poe {:http-status :result
                          :http-result (get-in res [:quote])})}))

  (reg-event-fx
   ::bad-poe-result
   (fn [{:keys [:db]} [_ res]]
     (println "BAD" (:status-text res))
     {:db (assoc db :poe {:http-status :error
                          :http-error "error"})}))

  (reg-event-fx
   ::reget-poe
   (fn [{:keys [:db]} _]
     {:db (assoc db :poe {:http-status :empty})
      :dispatch [::get-poe]}))

  (reg-event-fx
   ::get-poe
   (fn [{:keys [:db]} _]
     #?(:cljs (if (= (get-in db [:poe :http-status]) :empty)
                {:db (-> db
                         (assoc :poe {:http-status :loading}))
                 :http-fx {:uri  "/api/poe"
                           :on-success [::good-poe-result]
                           :on-failure [::bad-poe-result]}}
                {})
        :clj {})))



  ;; Custom ajax effect
  ;;
  #?(:cljs
     (re-frame/reg-fx
      :http-fx
      (fn [{:keys [token] :as options}]
        (let [defaults     {:method          :get
                            :timeout         10000 ;; config/api-timeout
                            :format          (ajax/json-request-format)
                            :response-format (ajax/json-response-format {:keywords? true})
                            :on-success      [::debug]
                            :on-failure      [::debug]}

              access-token (:access-token token)

              interceptors []
              interceptors (if access-token
                             (conj interceptors (ajax/to-interceptor {:name    "Token Interceptor"
                                                                      :request #(assoc-in % [:headers :Authorization] (str "Bearer " access-token))}))
                             interceptors)
              interceptors {:interceptors interceptors}

              options (merge defaults interceptors options)]

          (http-fx/http-effect options))))

     :clj
     (re-frame/reg-fx
      :http-fx
      (fn [{:keys [token] :as options}]
        (let [defaults {}
              access-token (:access-token token)

              options (assoc options :uri (str config/api-server (:uri options)))
              options (merge defaults options)]
          (try
            (let [response (http-client/get (:uri options))
                  json (cheshire.core/parse-string (:body response) true)]
              (re-frame/dispatch [(first (:on-success options)) json]))
            (catch Exception err
              (println "CLJ API ERROR" err)
              ;; (re-frame/dispatch [(first (:on-failure options)) err])
              ))))))

;;
)
