(ns mount-boot-srv.macro
  #?(:clj (:require [clojure.java.io :as io]
                    [clojure.edn :as edn])))

#?(:clj
   (defmacro slurp-translation [lang]
     (let [path (str "gettext/" (name lang) ".edn")]
       ;;(println "slurping translation" lang)
       (-> (io/resource path)
           (slurp)
           (edn/read-string)))))
