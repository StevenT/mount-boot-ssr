(ns mount-boot-srv.systems
  (:require [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [mount-boot-srv.api-routes :refer [api-routes]]
            [mount-boot-srv.site-routes :refer [site-routes]]
            [mount-boot-srv.db :refer [create-db]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [ring.middleware.defaults :refer [wrap-defaults
                                              site-defaults
                                              api-defaults]]
            (system.components
             [http-kit :refer [new-web-server]]
             [endpoint :refer [new-endpoint]]
             [middleware :refer [new-middleware]]
             [repl-server :refer [new-repl-server]]
             ;; [postgres :refer [new-postgres-database]]
             [jdbc :refer [new-database]]
             [handler :refer [new-handler]])))

(def rest-middleware
  (fn [handler]
    (wrap-restful-format handler
                         :formats [:json-kw]
                         :response-options {:json-kw {:pretty true}})))

(def db-spec
  {:classname "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname "database.sqlite3"})

(defn dev-system []
  (component/system-map
     :db (new-database db-spec create-db)

     :site-middleware (new-middleware {:middleware [[wrap-defaults site-defaults]]})

     :api-middleware (new-middleware {:middleware  [rest-middleware
                                                    [wrap-defaults api-defaults]]})

     :site-endpoint (component/using
                     (new-endpoint site-routes)
                     [:site-middleware])

     :api-endpoint (component/using
                    (new-endpoint api-routes)
                    [:db :api-middleware])

     :handler (component/using
               (new-handler)
               [:api-endpoint :site-endpoint])

     :web-server (component/using (new-web-server (Integer. (env :http-port)))
                                  [:handler])))

(defn prod-system
  "Assembles and returns components for a production deployment"
  []
  (merge (dev-system)
         (component/system-map
          :repl-server (new-repl-server (read-string (env :repl-port))))))
