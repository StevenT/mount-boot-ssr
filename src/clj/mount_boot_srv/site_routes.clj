(ns mount-boot-srv.site-routes
  (:require [compojure.core :refer [GET routes]]
            [compojure.route :as route]
            [clojure.core.async :refer [chan]]
            [org.httpkit.server :refer [with-channel send! close]]
            [mount-boot-srv.render :as render]))

(defn site-routes [_]
  (routes
   (route/resources "/")
   (GET "*" []
        (fn [request]
          (let [channel (chan)]
            (with-channel request channel
              (render/render-page request #(send! channel %) (fn [_] (close channel)))))))))
