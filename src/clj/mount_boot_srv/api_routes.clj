(ns mount-boot-srv.api-routes
  (:require [compojure.api.sweet :as api]
            [ring.util.http-response :as response]
            [markdown.core :as md]
            [schema.core :as s]
            [mount-boot-srv.db :refer [get-users get-user]]))

(def readme-md (slurp "README.md"))
(def poe-quote "The now, dreary, borrow. nodded, silence Lenore! distinctly wished purple and Lenore! a 'Tis And my one my the at nothing the no said the separate Once the gently upon the this, dreaming the stood beating my dreams Darkness I the word door. upon of Lenore. and chamber Madam, came Only faintly door. longer. Merely dying the heart, still remember pondered, I angels This so was fantastic of door. more. came whispered or the with entrance quaint weak tapping, door. I morrow;—vainly Presently fact and to long radiant visiter my more. And came for nothing you, it token. sad for there Nameless the I, sorrow—sorrow more.")

(s/defschema User
  {:id s/Int
   :first_name s/Str
   :last_name s/Str
   (s/optional-key :title) s/Str
   (s/optional-key :description) s/Str
   (s/optional-key :created_at) s/Str
   (s/optional-key :updated_at) s/Str
   })


(defn api-routes [{:keys [db]}]
  (api/api
   {:swagger
    {:ui "/api-docs"
     :spec "/swagger.json"
     :options {:ui {:docExpansion "list"}}
     :data {:info {:title "Mount Boot Api"
                   :description "Mount, Boot, Compojure Api example"}
            :tags [{:name "api", :description "some apis"}]
            :consumes ["application/json"]
            :produces ["application/json"]}}}

   (api/context "/api" []
                :tags ["api"]

                (api/GET "/hello" []
                         :return {:msg String}
                         :summary "Hello World example"
                         (response/ok {:msg "Hello world!!"}))

                (api/GET "/poe" []
                         :return {:quote String}
                         :summary "Edgar Allen Poe like quote"
                         (response/ok {:quote poe-quote}))

                (api/GET "/readme" []
                         :return {:readme String}
                         :summary "README.md file as html"
                         (response/ok {:readme (md/md-to-html-string readme-md)}))

                (api/GET "/users" []
                         ;; :return {:users [User]}
                         :summary "users list in json"
                         (let [users (get-users db)]
                           (response/ok {:users users})))
                (api/GET "/user/:id" [id]
                         ;; :return {:user User}
                         :summary "user in json"
                         (let [user (get-user db id)]
                           (response/ok {:user user})))

                (api/GET "/test" []
                         :return {:url String}
                         :summary "Test quote"
                         (response/ok {:url (md/md-to-html-string "Hello **Utrecht**!\n\nGreetings from the Node API server")}))

                )
   )
  )
