(ns mount-boot-srv.render
  (:require [hiccup.core :refer [html]]
            [hiccup.page :refer [html5 include-css include-js]]
            [clojure.core.async :refer [go go-loop close! <! <!! >! chan alt! alt!! put! timeout]]
            [re-frame.core :as re-frame]
            [mount-boot-srv.client.config :as config]
            [mount-boot-srv.client.core :as core]
            [mount-boot-srv.client.subs :as subs]
            [mount-boot-srv.client.views :as views]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (defn react-id-str [react-id]
;;   (assert (vector? react-id))
;;   (str "." (clojure.string/join "." react-id)))

;; (defn set-react-id [react-id element]
;;   (update element 1 merge {:data-reactid (react-id-str react-id)}))

(defn normalize [component]
  (if (map? (second component))
    component
    (into [(first component) {}] (rest component))))

(defn render
  ([component] (render [0] component))
  ([id component]
   (cond
     (fn? component)
     (render (component))

     (not (coll? component))
     component

     (coll? (first component))
     (map-indexed #(render (conj id %1) %2) component)

     (keyword? (first component))
     (let [[tag opts & body] (normalize component)]
       (->> body
            (map-indexed #(render (conj id %1) %2))
            (into [tag opts])
            ;; (set-react-id id)
            ))

     (fn? (first component))
     (render id (apply (first component) (rest component))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn template [{:keys [body state url]}]
  (let  [new-title       (if (:title state) (str config/default-title " - " (:title state)) config/default-title)
         new-description (if (:description state) (:description state) config/default-description)
         lang            "nl" ;;(name (:language state))
         og              (:og state)
         og-title        (or (:title og) new-title)
         og-type         (or (:type og) "article")
         og-url          (or (:url og) url)
         og-image        (or (:image og) config/default-image)
         og-description  (or (:description og) new-description)]
    (html
     (html5
      {:lang lang}
      [:head
       [:title new-title]

       [:meta {:charset "utf-8"}]
       [:meta {:name    "viewport"
               :content "width=device-width, initial-scale=1.0"}]

       [:meta {:name    "description"
               :content new-description}]
       [:meta {:property "og:title"
               :content  og-title}]
       [:meta {:property "og:type"
               :content  og-type}]
       [:meta {:property "og:url"
               :content  og-url}]
       [:meta {:property "og:image"
               :content  og-image}]
       [:meta {:property "og:description"
               :content  og-description}]

       (include-css "/css/main.css")

       [:link {:rel "manifest"
               :href "/manifest.json"}]
       [:link {:rel   "apple-touch-icon"
               :sizes "180x180"
               :href  "/apple-touch-icon.png"}]
       [:link {:rel   "icon"
               :type  "image/png"
               :sizes "32x32"
               :href  "/favicon-32x32.png"}]
       [:link {:rel   "icon"
               :type  "image/png"
               :sizes "16x16"
               :href  "/favicon-16x16.png"}]
       [:link {:rel   "mask-icon"
               :href  "/safari-pinned-tab.svg"
               :color "#3f6f7d"}]
       [:meta {:name    "msapplication-TileColor"
               :content "#00aba9"}]
       [:meta {:name    "theme-color"
               :content "#ffffff"}]]

      [:body
       [:div#app body]
       [:div#server-data {:style "display: none;"
                          :data-state (pr-str state)}]
       (include-js "/main.js")]))))

(def ssr-timeout 2000)

(defn renderit [url]
  (let [db (re-frame/subscribe [::subs/db])]
    (template {:body  (render views/page)
               :state @db
               :url   url})))

(defn subscribe-chan [render-chan stop-chan subscription]
  (let [waits  (re-frame/subscribe subscription)
        ms-inc 10]
    (go-loop []
      (when (alt! stop-chan false
                  :default :keep-going)
        (if @waits
          (do
            (println "done!")
            (>! render-chan :done))
          (do
            (println "waiting...")
            (<! (timeout ms-inc))
            (recur)))))))

(defn render-page [request respond _]
   (let [url "myURL"
         path (:uri request)
         render-chan (chan)
         stop-chan (chan)
         lang (get-in (:cookies request) ["language" :value])]
     (println "RENDERING in language" lang)
     (core/init {:path path :lang (keyword lang)})
     (go
       (subscribe-chan render-chan stop-chan [::subs/ssr-waits])
       (alt!
         render-chan :result
         (timeout ssr-timeout) (do
                                 (println :timeout)
                                 (put! stop-chan :stop)))
       (respond (renderit url)))))


