(ns mount-boot-srv.core
  (:gen-class)
  (:require [system.repl :refer [set-init! start]]
            [mount-boot-srv.systems :refer [prod-system]]))

(defn -main
  "Start the application"
  []
  (set-init! #'prod-system)
  (start))
