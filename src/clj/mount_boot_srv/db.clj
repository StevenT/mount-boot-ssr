(ns mount-boot-srv.db
  (:require [clojure.java.jdbc :as jdbc]))

(defn create-db
  "create db and table"
  [db]
  (println "Migrating Database:"  db)
  (try

    (jdbc/db-do-commands db
                         [(jdbc/drop-table-ddl :users)
                          (jdbc/create-table-ddl :users
                                                 [[:id :integer :primary :key]
                                                  [:first_name :text]
                                                  [:last_name :text]
                                                  [:title :text]
                                                  [:description :text]
                                                  [:created_at :datetime :default :current_timestamp]
                                                  [:updated_at :datetime :default :current_timestamp]])
                          "CREATE TRIGGER users_updated_at
    AFTER UPDATE ON users FOR EACH ROW BEGIN
      UPDATE users SET updated_at = current_timestamp
      WHERE id = old.id;
    END"]);


    (jdbc/insert-multi! db :users [{:first_name "Steven" :last_name "Thee" :title "Steven rocks!" :description "Dreaming my of Eagerly weary. and now, and Only the muttered, I This napping,
Over in radiant only Tis the door. implore. me, was I I, hesitating at quaint me
angels soul Ah, Some of there wrought surcease and darkness of suddenly a I
sought door. wished at visiter, uncertain the into my Lenore. gently And heart,
the grew dreams your came distinctly bleak before; murmured for I to many some
still whispered for darkness I and here nothing more. the From at the nothing
Presently nothing purple to floor. ever Sir, whom dream I before; nothing the
books tapping and at that borrow. and I Lenore! never the sure chamber of"}
                                   {:first_name "Titus" :last_name "Brandsma"}])
    (catch Exception e
      (println (.getMessage e))))
  )

(defn get-users [db]
  (jdbc/query (:db-spec db) "SELECT * FROM users"))

(defn get-user [db id]
  (jdbc/get-by-id (:db-spec db) :users id))
