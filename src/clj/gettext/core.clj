(ns gettext.core
  (:require [boot.core :refer [deftask]]
            [pottery.core :as pottery]
            [clojure.java.io :as io]))

(defn export-pot []
  (println "Pottery: exporting language files...")
  (pottery/scan-codebase!))

(def lang-path "resources/gettext/")

(defn import-pot [lang]
  (println (str "Pottery: importing language files for " lang "..."))
  (spit (str lang-path lang ".edn")
        (pottery/read-po-file (io/file (str lang-path lang ".po")))))

(deftask translate
  "translate pottery files"
  []
  (fn translate-middleware [next-handler]
    (fn translate-handler [fileset]
      ;; (export-pot)
      ;; (import-pot "nl")
      ;; (import-pot "fr")
      (next-handler fileset))))
