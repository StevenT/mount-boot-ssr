(ns user
  (:require [system.repl :refer [system set-init! start stop reset]]
            [mount-boot-srv.systems :refer [dev-system]]))

(set-init! #'dev-system)

