(def project 'mount-boot-srv)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"src/clj" "src/cljc" "src/cljs" "src/sass" "resources"}
          :dependencies   '[[org.clojure/clojure "1.10.0"]
                            [org.clojure/clojurescript "1.10.520"]

                            ;; Dev
                            [adzerk/boot-cljs "2.1.5" :scope "test"]
                            [adzerk/boot-cljs-repl "0.4.0" :scope "test"]
                            [adzerk/boot-reload "0.6.0" :scope "test"]
                            [adzerk/boot-test "1.2.0" :scope "test"]
                            [binaryage/devtools "0.9.10" :scope "test"]
                            [cider/piggieback "0.4.0" :scope "test"]
                            [deraen/boot-sass  "0.3.1" :scope "test"]
                            [nrepl "0.4.5" :scope "test"]
                            [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
                            [weasel "0.7.0" :scope "test"]

                            ;; Backend
                            [boot-environ "1.1.0"]
                            [cheshire "5.8.1"]
                            [clj-http "3.9.1"]
                            [compojure "1.6.1"]
                            [environ "1.1.0"]
                            [hiccup "1.0.5"]
                            [http-kit "2.3.0"]
                            [markdown-clj "1.0.7"]
                            [metosin/compojure-api "1.1.11"]
                            [metosin/ring-http-response "0.9.1"]
                            [org.clojure/core.async "0.4.490"]
                            [org.clojure/java.jdbc "0.7.9"]
                            [org.clojure/tools.cli "0.4.2"]
                            [org.clojure/tools.logging "0.4.1"]
                            [org.clojure/tools.nrepl "0.2.13"]
                            [org.danielsz/system "0.4.2"]
                            [org.immutant/immutant "2.1.10"]
                            [org.martinklepsch/boot-deps "0.1.10"]
                            [org.xerial/sqlite-jdbc "3.27.2.1"]
                            [ring "1.7.1"]
                            [ring-middleware-format "0.7.4"]
                            [ring/ring-defaults "0.3.2"]
                            [brightin/pottery "0.0.1"]
                            [prismatic/schema "1.1.11"]

                            ;; Frontend
                            [org.webjars.bower/bootstrap "4.3.1"]
                            [reagent "0.8.1"]
                            [re-frame "0.10.6"]
                            [day8.re-frame/http-fx "0.1.6"]
                            [bidi "2.1.5"]
                            [kibu/pushy "0.3.8"]
                            [com.cemerick/url "0.1.1"]
                            [devcards "0.2.5"] ;; pinned to 0.2.5 to prevent cljsjs.marked error
                            [org.clojars.unrealistic/re-frame-redux "0.1.0-SNAPSHOT"]
                            ])

(require '[system.boot :refer [system run]]
         '[mount-boot-srv.systems :refer [dev-system]]
         '[clojure.edn :as edn]
         '[environ.core :refer [env]]
         '[environ.boot :refer [environ]]
         '[adzerk.boot-cljs :refer :all]
         '[adzerk.boot-cljs-repl :refer :all]
         '[adzerk.boot-reload :refer :all]
         '[powerlaces.boot-cljs-devtools :refer [cljs-devtools]]
         '[deraen.boot-sass :refer [sass]]
         '[gettext.core :refer [translate import-pot export-pot]]
         )

(task-options!
 aot {:namespace   #{'mount-boot-srv.core}}
 jar {:main        'mount-boot-srv.core
      :file        (str "mount-boot-srv-" version "-standalone.jar")}
 repl {:init-ns 'user
       :eval '(set! *print-length* 20)})

(deftask pottery
  "process pottery translation files"
  []
  (export-pot)
  (import-pot "nl")
  (import-pot "fr"))

(deftask dev
  "run a restartable system"
  []

  (task-options!
   cljs {:optimizations    :none
         :compiler-options {:parallel-build  true
                            :optimizations   :none
                            :external-config {:devtools/config
                                              {:features-to-install [:formatters :hints]
                                               :fn-symbol           "λ"}}}})

  (comp
   (environ :env {:http-port "3000"})
   (watch :verbose true)
   (system :sys #'dev-system
           :auto true
           :mode :lisp
           :files ["api_routes.clj"
                   "core.clj"
                   "db.clj"
                   "render.clj"
                   "site_routes.clj"
                   "systems.clj"])
   (repl :server true
         ;; :host "127.0.0.1"
         )
   (reload :asset-path "public"
           :on-jsload 'mount-boot-srv.client/reload)
   (cljs-devtools)
   (cljs-repl)
   (cljs)
   (sass)
   (sift :move {#"main.css" "public/css/main.css"})
   (translate)
   (notify)))

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot)
          (pom)
          (uber)
          (jar)
          (target :dir dir))))

(deftask run-project
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[kongauth.core :as app])
  (apply (resolve 'app/-main) args))

(require '[adzerk.boot-test :refer [test]])
